import './App.css';

function App() {
  const subject = "React";
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Hello World!
        </p>
      </header>
    </div>
  );
}

export default App;